# Contributing

Thank you already to contributing to project! We trying make contributing as easier as possible. 

## Use Git Flow

Merge requests are the best way to propose changes to the codebase. We actively welcome your merge requests:

1. Fork the repo and create your branch from `main`
2. If you've added code that should be tested, add tests.
3. Ensure the tests pass. 
4. Make sure your code lints. 
6. Merge request

## Report Bugs

Report bugs in [issues](https://gitlab.com/mobilespectrum/MobileSpectrum/-/issues). Just create new issue and report details and steps to repeat the issue.